#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>

void peano(double x, double y, int width, double i, double j, double convertion_factor)
{
 if(width==1)
 {
  double x_converted = convertion_factor*x, y_converted = convertion_factor*y;
  std::cout<<x_converted<<" "<<y_converted<<" 0.1975 1200"<<std::endl;
  return;
 }
 width = width/3;
 peano(x+(2*i*width), y+(2*i*width), width, i, j,convertion_factor);
 peano(x+(i-j+1)*width, y+(i+j)*width, width, i, 1-j,convertion_factor);
 peano(x+width, y+width, width, i, 1-j,convertion_factor);
 peano(x+(i+j)*width, y+(i-j+1)*width, width, 1-i, 1-j,convertion_factor);
 peano(x+(2*j*width), y+2*(1-j)*width, width, i, j,convertion_factor);
 peano(x+(1+j-i)*width, y+(2-i-j)*width, width, i, j,convertion_factor);
 peano(x+2*(1-i)*width, y+2*(1-i)*width, width, i, j,convertion_factor);
 peano(x+(2-i-j)*width, y+(1+j-i)*width, width, 1-i, j,convertion_factor);
 peano(x+2*(1-j)*width, y+2*j*width, width, 1-i, j,convertion_factor);

}

int main()
{
 int num_pixels = 81;
 int peano_width = sqrt (num_pixels);
 double tile_size = 5;
 double peano_convert_fact = tile_size/(peano_width-1);
 peano(0,0,peano_width,0,0, peano_convert_fact);
}

