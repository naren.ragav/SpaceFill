#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
using namespace std;

void hilbert(double x_o, double y_o, int width, double x_1, double y_1, double convertion_factor, ofstream &output_file_1, ofstream &output_file_2, double &dummy, double on_time, double power)
{
 if(width==1)
 {
  double x_converted = convertion_factor*x_o, y_converted = convertion_factor*y_o;
  output_file_1<<x_converted<<" "<<y_converted<<" "<<on_time<<" "<<power<<std::endl;
  output_file_2<<x_converted<<" "<<y_converted<<" "<<dummy<<std::endl; 
  dummy += on_time;
  return;
 }
 width = width/2;
 hilbert(x_o+x_1*width, y_o+x_1*width, width, x_1, 1-y_1,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 hilbert(x_o+y_1*width, y_o+(1-y_1)*width, width, x_1, y_1,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 hilbert(x_o+(1-x_1)*width, y_o+(1-x_1)*width, width, x_1, y_1,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 hilbert(x_o+(1-y_1)*width, y_o+(y_1)*width, width, 1-x_1, y_1,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
}

void raster(double x_o, double y_o, int pixel_count_linear, double convert_fact, double on_time, double power)
{
  int direction_flag = 1;
  double dummy = on_time;
  std::ofstream data_file, data_file_1;
  data_file.open("raster_bidirectional.txt");
  data_file_1.open("bidirec_plot.txt");
  for(int i=0; i<pixel_count_linear; i++)
  {
    for(int j=0; j<pixel_count_linear; j++)
    {
      if(direction_flag==1) 
         {   
           data_file<<(x_o+j)*convert_fact<<" "<<(y_o + i)*convert_fact<<" "<<on_time<<" "<<power<<std::endl;
           data_file_1<<(x_o+j)*convert_fact<<" "<<(y_o + i)*convert_fact<<" "<<dummy<<std::endl;
           dummy += on_time;
         }
     if(direction_flag==0) 
         { 
           data_file<<(pixel_count_linear-j-1)*convert_fact<<" "<<(y_o + i)*convert_fact<<" "<<on_time<<" "<<power<<std::endl;
           data_file_1<<(pixel_count_linear-j-1)*convert_fact<<" "<<(y_o + i)*convert_fact<<" "<<dummy<<std::endl;
           dummy += on_time;
         }
    }
   direction_flag = (direction_flag + 1)%2;
  }
  data_file.close();
  data_file_1.close();
  
  dummy = on_time;
  data_file.open("raster_unidirectional.txt");
  data_file_1.open("unidirec_plot.txt");
  for(int i=0; i<pixel_count_linear; i++)
  {
    for(int j=0; j<pixel_count_linear; j++)
    {
      data_file<<(x_o+j)*convert_fact<<" "<<(y_o+i)*convert_fact<<" "<<on_time<<" "<<power<<std::endl;
      data_file_1<<(x_o+j)*convert_fact<<" "<<(y_o+i)*convert_fact<<" "<<dummy<<std::endl;
      dummy += on_time;
    }
   direction_flag = (direction_flag + 1)%2;
  }
  data_file.close();
}

void peano(double x, double y, int width, double i, double j, double convertion_factor, ofstream &output_file_1, ofstream &output_file_2, double &dummy, double on_time, double power)
{
 if(width==1)
 {
  double x_converted = convertion_factor*x, y_converted = convertion_factor*y;
  output_file_1<<x_converted<<" "<<y_converted<<" "<<on_time<<" "<<power<<std::endl;
  output_file_2<<x_converted<<" "<<y_converted<<" "<<dummy<<std::endl;
  dummy += on_time;
  return;
 }
 width = width/3;
 peano(x+(2*i*width), y+(2*i*width), width, i, j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 peano(x+(i-j+1)*width, y+(i+j)*width, width, i, 1-j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 peano(x+width, y+width, width, i, 1-j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 peano(x+(i+j)*width, y+(i-j+1)*width, width, 1-i, 1-j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 peano(x+(2*j*width), y+2*(1-j)*width, width, i, j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 peano(x+(1+j-i)*width, y+(2-i-j)*width, width, i, j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 peano(x+2*(1-i)*width, y+2*(1-i)*width, width, i, j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 peano(x+(2-i-j)*width, y+(1+j-i)*width, width, 1-i, j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 peano(x+2*(1-j)*width, y+2*j*width, width, 1-i, j,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);

}

void Lebesque(double x_o, double y_o, int width, double x_1, double y_1, double convertion_factor, ofstream &output_file_1, ofstream &output_file_2, double &dummy, double on_time, double power)
{
 if(width==1)
 {
  double x_converted = convertion_factor*x_o, y_converted = convertion_factor*y_o;
  output_file_1<<x_converted<<" "<<y_converted<<" "<<on_time<<" "<<power<<std::endl;
  output_file_2<<x_converted<<" "<<y_converted<<" "<<dummy<<std::endl;
  dummy += on_time;
  return;
 }
 width = width/2;
 Lebesque(x_o+x_1*width, y_o+x_1*width, width, x_1, y_1,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 Lebesque(x_o+y_1*width, y_o+(1-y_1)*width, width, x_1, y_1,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 Lebesque(x_o+(1-y_1)*width, y_o+(y_1)*width, width, x_1, y_1,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);
 Lebesque(x_o+(1-x_1)*width, y_o+(1-x_1)*width, width, x_1, y_1,convertion_factor, output_file_1, output_file_2, dummy, on_time, power);

}

int main()
{

 double on_time_1 = 0.25, power_1 = 1200; //on-time, power for 2^n pixel cases. Everything except peano curve.
 double on_time_2 = 0.1975, power_2=1200;
 ofstream output_file_1, output_file_2;
 double dummy;

 int num_pixels = 64;
 int hil_width = sqrt (num_pixels);
 double tile_size = 3;
 double hil_convert_fact = tile_size/(hil_width-1);
 dummy = on_time_1;
 output_file_1.open("hilbert.txt");
 output_file_2.open("hilbert_plot.txt");
 hilbert(0,0,hil_width,0,0, hil_convert_fact, output_file_1, output_file_2, dummy, on_time_1, power_1);
 output_file_1.close();
 output_file_2.close();
 
 
 int linear_pixel_count = 8;
 double raster_convert_fact = tile_size/(linear_pixel_count-1);
 raster(0,0,linear_pixel_count, raster_convert_fact, on_time_1, power_1);

 
 int peano_num_pixels = 81;
 int peano_width = sqrt (peano_num_pixels);
 double peano_convert_fact = tile_size/(peano_width-1);
 dummy = on_time_2;
 output_file_1.open("peano.txt");
 output_file_2.open("peano_plot.txt");
 peano(0,0,peano_width,0,0, peano_convert_fact, output_file_1, output_file_2, dummy, on_time_2, power_2);
 output_file_1.close();
 output_file_2.close();

 int lebes_width = sqrt (num_pixels);
 double lebes_convert_fact = tile_size/(lebes_width-1);
 output_file_1.open("lebesgue.txt");
 output_file_2.open("lebesgue_plot.txt");
 dummy = on_time_1;
 Lebesque(0,0,lebes_width,0,0, lebes_convert_fact, output_file_1, output_file_2, dummy, on_time_1, power_1);
 output_file_1.close();
 output_file_2.close();

}

