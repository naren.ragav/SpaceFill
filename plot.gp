set terminal png size 1000,1000 enhanced font "Helvetica,20"
set output 'PEANO_MEANDERED.png'
set title "Meandering PEANO Curve"
unset key
set xlabel "X (m)"
set ylabel "Y (m)"
set xrange[-0.001:0.006]
set yrange[-0.001:0.006]
plot "peano.txt" using 1:2 with lines

set terminal png size 1000,1000 enhanced font "Helvetica,20"
set output 'Hilbert.png'
set title "Hilbert Curve"
unset key
set xlabel "X (m)"
set ylabel "Y (m)"
set xrange[-0.001:0.006]
set yrange[-0.001:0.006]
plot "hilbert.txt" using 1:2 with lines

set terminal png size 1000,1000 enhanced font "Helvetica,20"
set output 'LEBESGUE.png'
set title "Lebesgue Curve"
unset key
set xlabel "X (m)"
set ylabel "Y (m)"
set xrange[-0.001:0.006]
set yrange[-0.001:0.006]
plot "lebesgue.txt" using 1:2 with lines

set terminal png size 1000,1000 enhanced font "Helvetica,20"
set output 'RASTER_BIDIRECTIONAL.png'
set title "Bidirectional Raster Melt"
unset key
set xlabel "X (m)"
set ylabel "Y (m)"
set xrange[-0.001:0.006]
set yrange[-0.001:0.006]
plot "raster_bidirectional.txt" using 1:2 with lines

set terminal png size 1000,1000 enhanced font "Helvetica,20"
set output 'RASTER_UNIDIRECTIONAL.png'
set title "Unidirectional Raster Melt"
unset key
set xlabel "X (m)"
set ylabel "Y (m)"
set xrange[-0.001:0.006]
set yrange[-0.001:0.006]
plot "raster_unidirectional.txt" using 1:2 with lines

