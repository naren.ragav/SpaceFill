#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>

void hilbert(double x_o, double y_o, int width, double x_1, double y_1, double convertion_factor)
{
 if(width==1)
 {
  double x_converted = convertion_factor*x_o, y_converted = convertion_factor*y_o;
  std::cout<<x_converted<<" "<<y_converted<<" 0.25 1200"<<std::endl;
  return;
 }
 width = width/2;
 hilbert(x_o+x_1*width, y_o+x_1*width, width, x_1, 1-y_1,convertion_factor);
 hilbert(x_o+y_1*width, y_o+(1-y_1)*width, width, x_1, y_1,convertion_factor);
 hilbert(x_o+(1-x_1)*width, y_o+(1-x_1)*width, width, x_1, y_1,convertion_factor);
 hilbert(x_o+(1-y_1)*width, y_o+(y_1)*width, width, 1-x_1, y_1,convertion_factor);

}

void raster(double x_o, double y_o, int pixel_count_linear, double convert_fact)
{
  int direction_flag = 1;
  std::ofstream data_file;
  data_file.open("raster_bidirectional.txt");
  for(int i=0; i<pixel_count_linear; i++)
  {
    for(int j=0; j<pixel_count_linear; j++)
    {
      if(direction_flag==1) data_file<<(x_o+j)*convert_fact<<" "<<(y_o + i)*convert_fact<<" 0.25 1200"<<std::endl;
      if(direction_flag==0) data_file<<(pixel_count_linear-j-1)*convert_fact<<" "<<(y_o + i)*convert_fact<<" 0.25 1200"<<std::endl;
    }
   direction_flag = (direction_flag + 1)%2;
  }
  data_file.close();
  
  data_file.open("raster_unidirectional.txt");
  for(int i=0; i<pixel_count_linear; i++)
  {
    for(int j=0; j<pixel_count_linear; j++)
    {
      data_file<<(x_o+j)*convert_fact<<" "<<(y_o+i)*convert_fact<<" 0.25 1200"<<std::endl;
    }
   direction_flag = (direction_flag + 1)%2;
  }
  data_file.close();
}

int main()
{
 int num_pixels = 64;
 int hil_width = sqrt (num_pixels);
 double tile_size = 5;
 double hil_convert_fact = tile_size/(hil_width-1);
 hilbert(0,0,hil_width,0,0, hil_convert_fact);
 int linear_pixel_count = 8;
 double raster_convert_fact = tile_size/(linear_pixel_count-1);
 raster(0,0,linear_pixel_count, raster_convert_fact);
}

