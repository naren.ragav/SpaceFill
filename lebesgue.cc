#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>

void Lebesque(double x_o, double y_o, int width, double x_1, double y_1, double convertion_factor)
{
 if(width==1)
 {
  double x_converted = convertion_factor*x_o, y_converted = convertion_factor*y_o;
  std::cout<<x_converted<<" "<<y_converted<<" 0.25 1200"<<std::endl;
  return;
 }
 width = width/2;
 Lebesque(x_o+x_1*width, y_o+x_1*width, width, x_1, y_1,convertion_factor);
 Lebesque(x_o+y_1*width, y_o+(1-y_1)*width, width, x_1, y_1,convertion_factor);
 Lebesque(x_o+(1-y_1)*width, y_o+(y_1)*width, width, x_1, y_1,convertion_factor);
 Lebesque(x_o+(1-x_1)*width, y_o+(1-x_1)*width, width, x_1, y_1,convertion_factor);

}

int main()
{
 int num_pixels = 64;
 int lebes_width = sqrt (num_pixels);
 double tile_size = 5;
 double lebes_convert_fact = tile_size/(lebes_width-1);
 Lebesque(0,0,lebes_width,0,0, lebes_convert_fact);
}

