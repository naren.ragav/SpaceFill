#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <vector>
using namespace std;
void layer(double X_ORIGIN, double Y_ORIGIN, double DX, double DY, int X_PIXEL_SKIP, int Y_PIXEL_SKIP, double X_LENGTH, double Y_LENGTH, int BI_DIRECTION_FLAG, std::vector<double> &X_coord, std::vector<double> &Y_coord)
{
  std::ofstream data_file;
  data_file.open("xyzdtp.dat");
  double x_temp = X_ORIGIN, y_temp = Y_ORIGIN;
  int x_pixel_count = X_LENGTH/DX+1, y_pixel_count = Y_LENGTH/DY+1, total_pixels = x_pixel_count*y_pixel_count;

  if(BI_DIRECTION_FLAG==0)
  {
    for(int k=0; k<=Y_PIXEL_SKIP; k++)
    {
     y_temp = Y_ORIGIN +Y_LENGTH-k*DY;
     for(int j=0; j<=y_pixel_count; j++)
     { 
      
      for(int i=0; i<=X_PIXEL_SKIP;  i++)
      {
       x_temp = X_ORIGIN +i*DX;
       for(int h=0; h<=x_pixel_count; h++)
         {
           if(((abs(abs(X_LENGTH)+abs(X_ORIGIN))>=abs(x_temp))&&(abs(x_temp)>=abs(X_ORIGIN))&&(abs(abs(abs(Y_LENGTH)+abs(Y_ORIGIN)))>=abs(y_temp))&&(abs(y_temp)>=abs(Y_ORIGIN)))) 
              { data_file<<x_temp<<" "<<y_temp<<std::endl;  X_coord.push_back(x_temp); Y_coord.push_back(y_temp);}           
           x_temp = x_temp + (X_PIXEL_SKIP+1)*DX;
         }
       }
        y_temp = y_temp - (Y_PIXEL_SKIP+1)*DY;
      }
     }
    data_file.close();
    return void();
  }


  if(BI_DIRECTION_FLAG==1)
  {
     for(int j=0; j<=y_pixel_count; j++)
     {
       y_temp = Y_ORIGIN + Y_LENGTH - j*DY;
       for(int h=0; h<=x_pixel_count; h++)
         {
           if(j%2==0) x_temp = X_ORIGIN + h*DX; else x_temp = X_ORIGIN + X_LENGTH - h*DX;
           if(((X_LENGTH+X_ORIGIN>=x_temp)&&(x_temp>=X_ORIGIN)&&((Y_LENGTH+Y_ORIGIN)>=y_temp)&&(y_temp>=Y_ORIGIN))) 
                  {
                    data_file<<x_temp<<" "<<y_temp<<std::endl;
                    X_coord.push_back(x_temp); Y_coord.push_back(y_temp);
                  }
         }
      }
   
    data_file.close();
    return void();
  }
   
}

void ptg(std::vector<double> X_coord, std::vector<double> Y_coord, double Z_HEIGHT, double LAYER_THICKNESS, double X_ORIGIN, double Y_ORIGIN, double X_LENGTH, double Y_LENGTH)
{
 int num_layers = Z_HEIGHT/LAYER_THICKNESS;
 int count=X_coord.size();
 cout<<num_layers<<endl; 
 std::ofstream data_file;
 data_file.open("test.ptg");
 data_file<<num_layers<<endl<<endl;
 for(int i=0;i<num_layers;i++)
  {
    data_file<<(i+1)*LAYER_THICKNESS<<endl;
    for(int j=0;j<count;j++)
    {
      data_file<<X_coord.at(j)<<" "<<Y_coord.at(j)<<endl;
    }
     data_file<<endl;    
  }
 data_file<<X_ORIGIN<<" "<<Y_ORIGIN<<" "<<LAYER_THICKNESS<<" "<<X_ORIGIN+X_LENGTH<<" "<<Y_ORIGIN+Y_LENGTH<<" "<<Z_HEIGHT<<endl;
 data_file.close();
 return void();
}

int main()
{
 double X_origin, Y_origin, X_length, Y_length, z_height, layer_thickness, x_linear_pixel_density, y_linear_pixel_density;
 int X_pixel_skip, Y_pixel_skip;
 int bi_direction_flag = 0, layer_offset_flag =0;
 std::vector<double> X;
 std::vector<double> Y;
 cout<<std::endl<<"Enter X-origin of the part in mm: "; cin>>X_origin;
 cout<<std::endl<<"Enter Y-origin of the part in mm: "; cin>>Y_origin;
 cout<<std::endl<<"Enter length of part along X-axis in mm: "; cin>>X_length;
 cout<<std::endl<<"Enter length of the part along Y-axis in mm: "; cin>>Y_length;
 cout<<endl<<"Enter height of part in mm: ";cin>>z_height;
 cout<<endl<<"Enter the layer thickness in mm: ";cin>>layer_thickness;
 cout<<std::endl<<"Enter the linear pixel density (px/mm) in X-axis:"; cin>>x_linear_pixel_density;
 cout<<std::endl<<"Enter the linear pixel density (px/mm) in Y-axis:"; cin>>y_linear_pixel_density;
 cout<<std::endl<<"Enter 1 if you want bi-directional raster (Use with X-pixel-skip and Y-pixel-skip as 0) else enter 0: "; cin>>bi_direction_flag;
 cout<<std::endl<<"Enter pixel skip along X-axis: "; cin>>X_pixel_skip;
 cout<<std::endl<<"Enter pixel skip along Y-axis: "; cin>>Y_pixel_skip;
 //cout<<endl<<"Enter 1 if you want to turn on ABC offset between layers else enter 0: "; cin>>layer_offset_flag;
 double dx = (1.0/x_linear_pixel_density), dy = (1.0/y_linear_pixel_density);
 layer(X_origin, Y_origin, dx, dy, X_pixel_skip, Y_pixel_skip, X_length, Y_length, bi_direction_flag, X, Y);
 ptg(X,Y,z_height,layer_thickness,X_origin,Y_origin,X_length,Y_length);
}

